# @hbieshaar/publisher

This module implements a publisher/subscription pattern with a twist.

The main addition to the pattern is that the `subscribe()` function has an additional parameter `trigger`. When `trigger` is set to `true`, a subscription will immediately trigger a 'resend' of the last publish data on the topic being subscrribed to.

## create a `Publisher`
A Publisher can be created as a base-class, by a mixin to a class, or for a specific object.

### base-class
The use as a base-class will not be common.
``` js
function DerivedClass() {
    Publisher.call(this);
}

Object.setPrototypeOf(DerivedClass.prototype, Publisher.prototype);
```

### mixin on a class
The most common use will be as a mixin.
``` js
function MixedInClass() {
    Publisher.call(this);
}

Publisher.addTo(MixedInClass);
```

### mixin on an object
Changing an object into a Publisher is not expected to be common.
``` js
const obj = { data: 3 };

Publisher.publishify(obj);
```

## use a `Publisher`
Most of the functions in the publisher deal with the subscription to
a topic. In the following examples, we assume there are 2 objects:

 - `pub` the publisher object, and
 - `sub` the subscriber object, and

a topic 'my data', whish returns a single value.

### `publish(topic, ...data)`
``` js
pub.publish('my data', 4);
```
This call publishes the topic 'my data' to all subscriptions. It also stores that data in case a new subscription triggers
a resend. So, this call might be usefull even if it is known
that there can not be any subscribers yet.

### `subscribe(topic, callback, trigger=false)`
Subscribe to a topic using
``` js
// create and save the callback
sub.cb = x => console.log('Just received', x);

pub.subscribe('my data', sub.cb, true);
// expect output
// Just received 4
```
The advantage of using `trigger == true` is an immediate initatization
of the loosely coupled system.

### `unsubscribe(topic, callback)`
To unsubscribe
``` js
pub.unsubscribe('my data', sub.cb);
```

### `once(topic, callback, trigger=false)`
To subscribe to a topic for **one** publication
``` js
pub.once('my data', x => console.log('At last', x));
// expect nothing because `trigger == false`

pub.publish('my data', 5);
// expect output
// At last 5

pub.publish('my data', 6);
// expect nothing, there are no subscriptions anymore.
```

## Further Documentation

The code in the file is easily readable, and well documented. The
BitBucket repository has a JSDoc website.
