/**
 * The topicCallback describes a function that may be used
 * to subscribe to a topic.
 * @callback topicCallback
 * @param {any[]} data - the data that is published by a call
 * to publish().
 */

/**
 * The Topic type contains a list of callbacks from subsribers,
 * and the data of last publication for the topic.
 * @typedef {Object} Topic
 * @property {any[]} data - the data that was last published
 * for a topic by a call to `publish()`
 * @property {topicCallback[]} list - the list of callbacks,
 * that need to be called when a topic is published.
 */

/**
 * Publisher: a simple publisher/subscriber implemenation.
 * 
 * This class can be used as a base class, and as a mixin.
 * To use as a mixin assign the prototype to the prototype
 * of the target class/function (using the static function
 * Publisher.addTo(cls)), and call the constructor
 * using 'Publisher.call(this)'.
 * @constructor
 */
export function Publisher() {

    /**
     * An object, whose keys are the topics that have been
     * subscribed to, with a contents of an array to callback
     * functions that are to be called when that topic gets
     * published again.
     * @type {Object.<string, Array.<{Topic}>>}
     */
    this._topics = Object.create(null);
}

Object.assign(Publisher.prototype, /** @lends Publisher.prototype */{

    /**
     * Add a user callback to the topic list
     * @param {string} topic - the topic to subscribe to
     * @param {topicCallback} callback - to callback that needs to be
     * called on each publication of the topic.
     * @param {boolean=} trigger - immediately trigger a
     *      subscribing callback (default: false)
     */
    subscribe(topic, callback, trigger=false) {
        if ( ! (topic in this._topics) )
            this._topics[topic] = { data: [], list: [] };

        this._topics[topic].list.push(callback);

        if (trigger)
            callback(...(this._topics[topic].data));
    },

    /**
     * Remove a user callback from the topic list
     * @param {string} topic - the topic to unsubscribe
     * @param {topicCallback} callback - to callback that needs to be
     * removed from the subscription list.
     */
    unsubscribe(topic, callback) {
        if ( ! (topic in this._topics) ) return;

        this._topics[topic].list = this._topics[topic].list.filter(
            cb => cb !== callback
        );
    },

    /**
     * Add a user callback to the `_topics` list for a single
     * publication.
     * @param {string} topic - the topic to subscribe to
     * @param {topicCallback} callback  - the callback that needs to be
     * called on the first publication of the topic
     * @param {boolean=} trigger - immediately trigger a
     *      subscribing callback (default: false)
     */
    once(topic, callback, trigger=false) {
        const g = () => {
            this.unsubscribe(topic, g);
            callback.call(this, ...arguments);
        };
        this.subscribe(topic, g, trigger);
    },

    /**
     * Publish a topic
     * @param {string} topic - the topic to subscribe to
     * @param {...any} data - the data that is published
     */
    publish(topic, ...data) {
        if ( ! (topic in this._topics) )
            this._topics[topic] = { data, list: [] };
        else
            this._topics[topic].data = data;

        this._topics[topic].list.forEach(cb => cb(...data));
    },

});

/**
 * Mixes the Publisher.prototype into the target classes prototype.
 * @param {function} cls the (function) class to mixin with
 */
Publisher.addTo = function (cls) {
    const proto = cls.prototype;

    Object.assign(proto, Publisher.prototype);
};

/**
 * Make an object into a publisher
 * @param {object} obj - the object that is to be changed into
 * a publisher.
 */
Publisher.publishify = function (obj) {
    obj._topics = {};
    Object.assign(obj, Publisher.prototype);
}
